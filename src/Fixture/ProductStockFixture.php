<?php declare(strict_types=1);

namespace App\Fixture;

use App\Entity\Product\ProductStockStatus;
use App\Manager\ProductStockManager;
use Doctrine\Persistence\ObjectManager;
use Sylius\Bundle\FixturesBundle\Fixture\AbstractFixture;

class ProductStockFixture extends AbstractFixture
{
    private ObjectManager $productManager;
    private ProductStockManager $productStockManager;

    public function __construct(ObjectManager $productManager, ProductStockManager $productStockManager)
    {
        $this->productManager = $productManager;
        $this->productStockManager = $productStockManager;
    }

    public function load(array $options): void
    {
        $this->loadProductStockStatus();
        $this->loadProductStock();
    }

    private function loadProductStock()
    {
        $this->productStockManager->fillProductStock();
    }

    private function loadProductStockStatus(): void
    {
        foreach ($this->getProductStockStatusData() as $productStockStatusData) {
            $productStockStatus = new ProductStockStatus();
            $productStockStatus->setAlias($productStockStatusData['alias']);
            $productStockStatus->setName($productStockStatusData['name']);

            $this->productManager->persist($productStockStatus);
        }

        $this->productManager->flush();
    }

    public function getName(): string
    {
        return 'product_stock_status';
    }

    private function getProductStockStatusData(): array
    {
        return [
            [
                'alias' => ProductStockStatus::AVAILABLE,
                'name' => 'Available',
            ],
            [
                'alias' => ProductStockStatus::UNAVAILABLE,
                'name' => 'Unavailable',
            ],
            [
                'alias' => ProductStockStatus::ON_HOLD,
                'name' => 'On hold',
            ]
        ];
    }
}
