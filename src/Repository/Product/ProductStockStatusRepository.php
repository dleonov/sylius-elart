<?php

namespace App\Repository\Product;

use App\Entity\Product\ProductStockStatus;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<ProductStockStatus>
 *
 * @method ProductStockStatus|null find($id, $lockMode = null, $lockVersion = null)
 * @method ProductStockStatus|null findOneBy(array $criteria, array $orderBy = null)
 * @method ProductStockStatus[]    findAll()
 * @method ProductStockStatus[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProductStockStatusRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ProductStockStatus::class);
    }

    public function getStatus(string $status): ?ProductStockStatus
    {
        return $this->createQueryBuilder('pss')
            ->where('pss.alias = :status')
            ->setParameter('status', $status)
            ->getQuery()
            ->getOneOrNullResult();
    }
}
