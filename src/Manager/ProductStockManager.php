<?php declare(strict_types=1);

namespace App\Manager;

use App\Entity\Product\Product;
use App\Entity\Product\ProductStock;
use App\Entity\Product\ProductStockStatus;
use App\Entity\Product\ProductVariant;
use App\Repository\Product\ProductStockStatusRepository;
use Doctrine\ORM\EntityManagerInterface;
use Sylius\Component\Product\Repository\ProductRepositoryInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Contracts\Service\ServiceSubscriberInterface;
use Symfony\Contracts\Service\ServiceSubscriberTrait;

class ProductStockManager implements ServiceSubscriberInterface
{
    use ServiceSubscriberTrait;

    public static function getSubscribedServices(): array
    {
        return [
            EntityManagerInterface::class,
            ProductStockStatusRepository::class,
            ParameterBagInterface::class,
            'sylius.repository.product' => ProductRepositoryInterface::class,
        ];
    }

    public function toOnHoldStatus(ProductStock $productStock): void
    {
        $onHoldStatus = $this->getProductStockStatusRepository()
            ->getStatus(ProductStockStatus::ON_HOLD);

        $modifyDate = $this->getParameterBag()->get('restock_modify_date');
        $restockDate = (new \DateTimeImmutable())->modify($modifyDate);

        $productStock->setProductStockStatus($onHoldStatus);
        $productStock->setRestockDate($restockDate);

        $this->getEntityManager()->persist($productStock);
        $this->getEntityManager()->flush();
    }

    public function fillProductStock(): void
    {
        $products = $this->getProductRepository()->findAll();
        if (!$products) {
            throw new \RuntimeException('No products!');
        }

        $availableStatus = $this->getProductStockStatusRepository()
            ->getStatus(ProductStockStatus::AVAILABLE);
        $unavailableStatus = $this->getProductStockStatusRepository()
            ->getStatus(ProductStockStatus::UNAVAILABLE);

        /** @var $product Product */
        foreach ($products as $product) {
            $status = $unavailableStatus;

            if (!$product->getVariants()->isEmpty()) {
                /** @var $variant ProductVariant */
                foreach ($product->getVariants()->getIterator() as $variant) {
                    if ($variant->isInStock()) {
                        $status = $availableStatus;
                        break 1;
                    }
                }
            }

            $productStock = $product->getProductStock() ?? new ProductStock();
            $productStock->setProductStockStatus($status);
            $productStock->setRestockDate(null);
            $product->setProductStock($productStock);

            $this->getEntityManager()->persist($product);
        }

        $this->getEntityManager()->flush();
    }

    private function getEntityManager(): EntityManagerInterface
    {
        return $this->container->get(EntityManagerInterface::class);
    }

    private function getProductStockStatusRepository(): ProductStockStatusRepository
    {
        return $this->container->get(ProductStockStatusRepository::class);
    }

    public function getParameterBag(): ParameterBagInterface
    {
        return $this->container->get(ParameterBagInterface::class);
    }

    public function getProductRepository(): ProductRepositoryInterface
    {
        return $this->container->get('sylius.repository.product');
    }
}
