<?php declare(strict_types=1);

namespace App\Command;

use App\Manager\ProductStockManager;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

// the name of the command is what users type after "php bin/console"
#[AsCommand(name: 'app:fill-product-stock')]
class FillProductStockCommand extends Command
{
    public function __construct(
        private ProductStockManager $productStockManager,
    ){
        parent::__construct();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        try {
            $this->productStockManager->fillProductStock();
            $output->writeln('Success!');

            return Command::SUCCESS;
        } catch (\Throwable $exception) {
            $output->writeln($exception->getMessage());

            return Command::INVALID;
        }
    }
}
