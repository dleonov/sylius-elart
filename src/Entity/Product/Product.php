<?php

declare(strict_types=1);

namespace App\Entity\Product;

use Doctrine\ORM\Mapping as ORM;
use Sylius\Component\Core\Model\Product as BaseProduct;
use Sylius\Component\Product\Model\ProductTranslationInterface;

/**
 * @ORM\Entity
 * @ORM\Table(name="sylius_product")
 */
#[ORM\Entity]
#[ORM\Table(name: 'sylius_product')]
class Product extends BaseProduct
{
    #[ORM\OneToOne(mappedBy: 'product', cascade: ['persist', 'remove'], orphanRemoval: true)]
    private ?ProductStock $productStock = null;

    protected function createTranslation(): ProductTranslationInterface
    {
        return new ProductTranslation();
    }

    public function getProductStock(): ?ProductStock
    {
        return $this->productStock;
    }

    public function setProductStock(ProductStock $productStock): static
    {
        // set the owning side of the relation if necessary
        if ($productStock->getProduct() !== $this) {
            $productStock->setProduct($this);
        }

        $this->productStock = $productStock;

        return $this;
    }
}
