<?php

namespace App\Entity\Product;

use App\Repository\Product\ProductStockRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Sylius\Component\Resource\Model\ResourceInterface;

/**
 * @ORM\Entity
 * @ORM\Table(name="product_stock")
 */
#[ORM\Table(name: 'product_stock')]
#[ORM\Entity(repositoryClass: ProductStockRepository::class)]
class ProductStock implements ResourceInterface
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\OneToOne(inversedBy: 'productStock', cascade: ['persist', 'remove'])]
    #[ORM\JoinColumn(nullable: false)]
    private ?Product $product = null;

    #[ORM\Column(type: Types::DATE_MUTABLE, nullable: true)]
    private ?\DateTimeInterface $restockDate = null;

    #[ORM\ManyToOne(inversedBy: 'productStocks')]
    #[ORM\JoinColumn(nullable: false)]
    private ?ProductStockStatus $productStockStatus = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getProduct(): ?Product
    {
        return $this->product;
    }

    public function setProduct(Product $product): static
    {
        $this->product = $product;

        return $this;
    }

    public function getRestockDate(): ?\DateTimeInterface
    {
        return $this->restockDate;
    }

    public function setRestockDate(?\DateTimeInterface $restockDate): static
    {
        $this->restockDate = $restockDate;

        return $this;
    }

    public function getProductStockStatus(): ?ProductStockStatus
    {
        return $this->productStockStatus;
    }

    public function setProductStockStatus(?ProductStockStatus $productStockStatus): static
    {
        $this->productStockStatus = $productStockStatus;

        return $this;
    }
}
