<?php declare(strict_types=1);

namespace App\Controller;

use App\Entity\Product\ProductStock;
use App\Manager\ProductStockManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;

class ProductStockController extends AbstractController
{
    public function __construct(private ProductStockManager $productStockManager)
    {
    }

    public function onHold(ProductStock $productStock): Response
    {
        $this->productStockManager->toOnHoldStatus($productStock);

        $this->addFlash('success', 'Status updated to "On hold"');

        return $this->redirectToRoute('app_admin_product_stock_index');
    }
}
